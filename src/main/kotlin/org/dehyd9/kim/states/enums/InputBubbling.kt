package org.dehyd9.kim.states.enums

enum class InputBubbling(val isInputHandled: Boolean) {
    INPUT_HANDLED(isInputHandled = true), INPUT_HANDLED_IN_PARENT(isInputHandled = false)
}