package org.dehyd9.kim.startup.modules

import dagger.Module
import dagger.Provides
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.registry.interfaces.impl.CommandRegistryServiceImpl
import javax.inject.Singleton

@Module
class CommandRegistryServiceModule {

    @Singleton @Provides fun providesCommandRegistryService(commandRegistryService: CommandRegistryServiceImpl
    ) :CommandRegistryService {
        return commandRegistryService
    }
}