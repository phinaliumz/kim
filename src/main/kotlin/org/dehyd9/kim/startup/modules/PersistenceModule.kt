package org.dehyd9.kim.startup.modules

import dagger.Module
import dagger.Provides
import org.dehyd9.kim.persistence.services.DatabaseManager
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.persistence.services.impl.DatabaseManagerImpl
import org.dehyd9.kim.persistence.services.impl.InsertionManagerServiceImpl
import org.dehyd9.kim.services.UndoService
import org.dehyd9.kim.sessions.SessionManager
import javax.inject.Singleton

@Module
class PersistenceModule {

    @Singleton @Provides fun provideInsertionManagerService(databaseManager: DatabaseManager,
                                                            undoService: UndoService)
            :InsertionManagerService {
        return InsertionManagerServiceImpl(databaseManager = databaseManager,
                undoService = undoService)
    }

    @Singleton @Provides fun provideDatabaseManager(databaseManager: DatabaseManagerImpl)
            :DatabaseManager = databaseManager
}