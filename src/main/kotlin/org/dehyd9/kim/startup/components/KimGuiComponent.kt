package org.dehyd9.kim.startup.components

import dagger.Component
import org.dehyd9.kim.gui.interfaces.KimGUI
import org.dehyd9.kim.startup.modules.*
import javax.inject.Singleton

@Singleton
@Component(modules = [ServicesModule::class, CommandRegistryServiceModule::class,
    ParsersModule::class, KimGuiModule::class, PersistenceModule::class,
    SessionModule::class, ShortcutServiceModule::class])
interface KimGuiComponent {
    fun instance() :KimGUI
}