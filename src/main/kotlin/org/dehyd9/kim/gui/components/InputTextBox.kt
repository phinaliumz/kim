package org.dehyd9.kim.gui.components

import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.gui2.Interactable
import com.googlecode.lanterna.gui2.TextBox
import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.apache.commons.text.StringTokenizer
import org.apache.commons.text.TextStringBuilder
import org.dehyd9.kim.commands.interfaces.impl.InputModeExecutableCommandImpl
import org.dehyd9.kim.commands.interfaces.impl.OpenFileExecutableCommandImpl
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.controllers.InputTextController
import org.dehyd9.kim.gui.enums.ControlCharacter
import org.dehyd9.kim.gui.renderers.KimTextBoxRenderer
import org.dehyd9.kim.persistence.dto.InsertionDTO
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.TextFetchService
import org.dehyd9.kim.services.UndoService
import org.slf4j.LoggerFactory

@ExperimentalCoroutinesApi
class InputTextBox (
        preferredSize: TerminalSize?,
        private val inputTextController: InputTextController,
        private val textFetchService: TextFetchService,
        private val insertionManagerService: InsertionManagerService,
        private val shortcutService: ShortcutService,
        private val undoService: UndoService)
            :TextBox(preferredSize, Style.MULTI_LINE) {

    private val listOfMovementKeys = listOf(
            KeyType.ArrowDown, KeyType.ArrowUp, KeyType.ArrowLeft, KeyType.ArrowRight, KeyType.Home, KeyType.End,
            KeyType.PageDown, KeyType.PageUp, KeyType.Escape
    )

    private val LOGGER = LoggerFactory.getLogger(InputTextBox::class.java)
    private val viewBufferChannel: Channel<KeyStroke>

    var textReadOnly = false

    companion object {
        const val DEFAULT_ROWS = 400
        const val DEFAULT_COLUMNS = 80
        const val LOAD_MORE_ROWS_WHEN_THIS_CLOSE_TO_END = 5
        const val DEFAULT_BUFFER_SIZE = 500
    }

    init {

        this.renderer = KimTextBoxRenderer()
        inputTextController.controllerForInputTextBox = this
        viewBufferChannel= inputTextController.getViewBufferChannel()

        setCaretChangeRowWhenRightArrowIsPressedAtEndOfRow()
        inputTextController.filePath?.let {
            addWrappedTextFromFilepath(it)
        }
    }

    fun getPanelWidthInColumns() :Int {
        return when {
            preferredSize != null -> preferredSize.columns
            else -> DEFAULT_COLUMNS
        }
    }

    override fun handleKeyStroke(keyStroke: KeyStroke?): Interactable.Result {

        if(textReadOnly) {
            if (keyStroke?.character != null) {
                when(shortcutService.getCommandNameFromShortCutKey(keyStroke.character.toString())) {
                    InputModeExecutableCommandImpl.getCommandName() -> {
                        return Interactable.Result.UNHANDLED
                    }
                    OpenFileExecutableCommandImpl.getCommandName() -> {
                        return Interactable.Result.UNHANDLED
                    }
                }
            }

            if(!isKeyStrokeMovementKey(keyStroke)) {
                return Interactable.Result.HANDLED
            }
        } else {
            keyStroke?.let {
                if (!listOfMovementKeys.contains(it.keyType)) {
                    runBlocking {
                        if (!viewBufferChannel.isClosedForSend) {
                            viewBufferChannel.send(it)
                        }
                    }
                }
            }
        }

        /*when(keyStroke?.keyType) {
            KeyType.Character -> {
                LOGGER.debug("Character ${keyStroke.character} entered on row ${caretPosition.row}, column ${caretPosition.column}")
                persistTextInsertionInCurrentPosition(keyStroke.character, caretPosition.row, caretPosition.column)
            }
            KeyType.Backspace, KeyType.Delete -> {
                LOGGER.debug("Character ${keyStroke.character} deleted on row ${caretPosition.row}, column ${caretPosition.column}")
                persistSpecialKeyCodeInsertion(ControlCharacter.DELETE_CHARACTER, caretPosition.row, caretPosition.column)
            }
            KeyType.Enter -> {
                LOGGER.debug("Line feed entered on row ${caretPosition.row}, column ${caretPosition.column}")
                persistSpecialKeyCodeInsertion(ControlCharacter.NEW_LINE, caretPosition.row, caretPosition.column)
            }
            KeyType.ArrowDown, KeyType.PageDown -> {
                if(isCaretPositionInDangerZone()) {
                    this.loadMoreLinesFromLowerBuffer()
                }
            }
            KeyType.ArrowUp, KeyType.PageUp -> {
                if(isCaretPositionInDangerZone()) {
                    this.loadMoreTextFromUpperBuffer()
                }
            }
            KeyType.Escape -> {
                return Interactable.Result.UNHANDLED
            }
        }*/

        return super.handleKeyStroke(keyStroke)
    }

    private fun persistTextInsertionInCurrentPosition(inserted: Char, row: Int, column: Int) {
        /*val insertionDTO = InsertionDTO(
                row = row,
                column = column,
                insertedValue = inserted.toString(),
                uuid = inputTextController.controllerForUUID,
                date = Date(),
                isCommandKeyCodeInsertion = false)
        GlobalScope.launch { persistTextChanges(insertionDTO) }*/
    }

    private suspend fun persistSpecialKeyCodeInsertion(controlCharacter: ControlCharacter, row: Int, column: Int) {
        /*delay(5L)
        val insertionDTO = InsertionDTO(
                row = row,
                column = column,
                insertedValue = controlCharacter.name,
                uuid = inputTextController.controllerForUUID,
                dateAsMilliseconds = Date().time,
                isCommandKeyCodeInsertion = true
        )
        persistKeyCodeChange(insertionDTO)*/

    }

    private fun persistTextChanges(insertionDTO: InsertionDTO) {
        this.insertionManagerService.persistInsertion(insertionDTO)
    }

    private fun persistKeyCodeChange(insertionDTO: InsertionDTO) {
        this.insertionManagerService.persistInsertion(insertionDTO)
    }

    private fun isKeyStrokeMovementKey(keyStroke: KeyStroke?) :Boolean {
        return listOfMovementKeys.contains(keyStroke?.keyType)
    }

    private fun setCaretChangeRowWhenRightArrowIsPressedAtEndOfRow() {
        this.isCaretWarp = true
    }

    private fun isCaretPositionInDangerZone() :Boolean {
        val caretRow = caretPosition.row
        return lineCount - caretRow < LOAD_MORE_ROWS_WHEN_THIS_CLOSE_TO_END
    }

    private fun loadMoreTextFromUpperBuffer() {
        // TODO
    }

    private fun loadMoreLinesFromLowerBuffer() {
        //addWrappedTextFromListOfLines(inputTextController.getLinesFromLowerBuffer())
    }

    private fun addWrappedTextFromListOfLines(listOfLines: List<String>) {
        for(line in listOfLines) {
            addLine(line.trimEnd())
        }
    }

    private fun addWrappedTextFromFilepath(filepath: String) {
        val panelWidthInColumns = getPanelWidthInColumns()
        val charChannel = textFetchService.retrieveCharsFromFilePath(DEFAULT_ROWS * panelWidthInColumns, filepath)
        var charCounter = 0
        var columnCounter = 0

        GlobalScope.launch {
            var textBuilder = TextStringBuilder(panelWidthInColumns)
            for (char in charChannel) {
                textBuilder.append(char)
                if(textBuilder.contains(System.lineSeparator()) || columnCounter >= panelWidthInColumns) {
                    val builtString = textBuilder.toString()
                    charCounter += builtString.length

                    val lineTokenizer = StringTokenizer(builtString, System.lineSeparator())
                    while (lineTokenizer.hasNext()) {
                        addLine(lineTokenizer.nextToken())
                    }
                    columnCounter = 0
                    textBuilder = TextStringBuilder((panelWidthInColumns))
                }
                columnCounter++
            }

            //inputTextController.fillLowerTextBufferFromFile(charCounter)
        }

    }
}
