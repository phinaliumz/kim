package org.dehyd9.kim.gui.themes

import com.googlecode.lanterna.TextColor
import com.googlecode.lanterna.graphics.SimpleTheme
import com.googlecode.lanterna.graphics.Theme

class BasicAllBlackTheme {

    companion object {
        fun getBasicAllBlackTheme(): Theme {
            return SimpleTheme(TextColor.ANSI.BLACK, TextColor.ANSI.BLACK)
        }
    }
}