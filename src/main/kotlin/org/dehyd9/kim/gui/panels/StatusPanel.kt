package org.dehyd9.kim.gui.panels

import com.googlecode.lanterna.gui2.Label
import com.googlecode.lanterna.gui2.Panel

class StatusPanel:Panel() {

    val statusLabel = Label("READY")

    init {
        this.addComponent(statusLabel)
    }

    fun inputTextBoxWritable() {
        updateStatusLabelTo("INSERT")
    }

    fun inputTextBoxReadOnly() {
        updateStatusLabelTo("READY")
    }

    private fun updateStatusLabelTo(to: String) {
        statusLabel.text = to
    }
}