package org.dehyd9.kim.gui.panels

import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.gui2.Panel
import com.googlecode.lanterna.input.KeyStroke
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.controllers.InputTextController
import org.dehyd9.kim.gui.enums.Mode
import org.dehyd9.kim.gui.themes.BasicGreenBlackTheme.Companion.getBasicGreenBlackTheme
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.TextFetchService
import org.dehyd9.kim.services.UndoService
import org.dehyd9.kim.sessions.SessionManager
import org.dehyd9.kim.states.State
import java.io.File

@ExperimentalCoroutinesApi
@ObsoleteCoroutinesApi
class MainPanel(
        private var screenColumns: Int,
        private var screenRows: Int,
        private val textFetchService: TextFetchService,
        private val insertionManagerService: InsertionManagerService,
        private val shortcutService: ShortcutService,
        private val undoService: UndoService,
        sessionManager: SessionManager) : Panel() {

    internal var currentState: State = KeyCommandState()
    internal var inputPanels = HashSet<InputPanel>()
    internal val indexOfVisibleInputPanel: Int
        get() = this.internalIndexOfVisibleInputPanel

    private var internalIndexOfVisibleInputPanel: Int = 0
    private val uuidForFirstNewFile = sessionManager.getUUIDForUnsavedFile()
    private val firstInputPanel = InputPanel(
            screenColumns = screenColumns,
            screenRows = screenRows,
            inputTextController = InputTextController(
                    controllerForUUID = uuidForFirstNewFile,
                    textFetchService = textFetchService,
                    insertionManagerService = insertionManagerService,
                    filePath = null
            ),
            insertionManagerService = insertionManagerService,
            shortcutService = shortcutService,
            undoService = undoService,
            textFetchService = textFetchService)

    var mode = Mode.KEY_COMMAND

    init {
        firstInputPanel.theme = getBasicGreenBlackTheme()
        firstInputPanel.setTextForInputPanel("kim - inspired by vim")
        this.addComponent(firstInputPanel)
        inputPanels.add(firstInputPanel)
    }

    fun changeInputPanelTextBoxSize(terminalSize: TerminalSize?) {

        if(terminalSize != null) {
            screenColumns = terminalSize.columns
            screenRows = terminalSize.rows
        }

        if(terminalSize != null) {
            inputPanels.elementAt(internalIndexOfVisibleInputPanel).setInputPanelPreferredSize(terminalSize)
        }
    }

    fun createNewInputPanelWithInputController(inputTextController: InputTextController) {
        val newInputPanel = InputPanel(screenColumns = screenColumns, screenRows = screenRows,
                inputTextController = inputTextController,
                insertionManagerService = insertionManagerService, shortcutService = shortcutService,
                undoService = undoService, textFetchService = textFetchService)
        newInputPanel.containsKimInspiredText = false
        inputPanels.add(newInputPanel)
        internalIndexOfVisibleInputPanel = inputPanels.indexOf(newInputPanel)
        this.removeComponent(firstInputPanel)
        this.addComponent(newInputPanel)
        newInputPanel.setFocusToInputText()
    }

    fun getActiveInputPanelTextController() :InputTextController? {
        return inputPanels.elementAt(internalIndexOfVisibleInputPanel).inputTextController
    }

    fun closeMainPanel() {
        inputPanels.forEach { it.inputTextController.disposeInputController() }
    }

    private fun generateTmpFileAsBackupFile(fileName: String): File = File(System.getProperty("user.dir"), fileName)

    override fun handleInput(keyStroke: KeyStroke): Boolean {
        return currentState.handle(keyStroke = keyStroke, mainPanel = this)
    }
}