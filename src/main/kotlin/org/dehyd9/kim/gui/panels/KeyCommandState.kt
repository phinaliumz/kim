package org.dehyd9.kim.gui.panels

import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType
import org.dehyd9.kim.gui.enums.Mode
import org.dehyd9.kim.states.State
import org.dehyd9.kim.states.enums.InputBubbling

class KeyCommandState : State {
    override fun handle(keyStroke: KeyStroke, mainPanel: MainPanel): Boolean {
        when(keyStroke.keyType) {
            KeyType.Character -> {
                when(keyStroke.character) {
                    'i' -> {
                        mainPanel.currentState = InputState()
                        mainPanel.mode = Mode.INPUT
                        mainPanel.inputPanels.elementAt(mainPanel.indexOfVisibleInputPanel).setInputTextWritable()
                        return InputBubbling.INPUT_HANDLED.isInputHandled
                    }
                    ':' -> {
                        mainPanel.currentState = TextCommandState()
                        mainPanel.mode = Mode.TEXT_COMMAND
                        return InputBubbling.INPUT_HANDLED_IN_PARENT.isInputHandled
                    }
                }
            }
        }

        return InputBubbling.INPUT_HANDLED_IN_PARENT.isInputHandled
    }
}