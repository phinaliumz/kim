package org.dehyd9.kim.gui.enums

enum class Mode {
    INPUT, TEXT_COMMAND, KEY_COMMAND
}