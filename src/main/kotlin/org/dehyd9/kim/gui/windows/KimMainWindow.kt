package org.dehyd9.kim.gui.windows

import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.gui2.BasicWindow
import com.googlecode.lanterna.gui2.Direction
import com.googlecode.lanterna.gui2.LinearLayout
import com.googlecode.lanterna.input.KeyStroke
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.interfaces.CommandResultListener
import org.dehyd9.kim.commands.interfaces.impl.OpenFileExecutableCommandImpl
import org.dehyd9.kim.commands.interfaces.impl.QuitExecutableCommandImpl
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.controllers.InputTextController
import org.dehyd9.kim.gui.enums.Mode
import org.dehyd9.kim.gui.panels.MainPanel
import org.dehyd9.kim.parsers.TextCommandParser
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.TextFetchService
import org.dehyd9.kim.services.UndoService
import org.dehyd9.kim.sessions.SessionManager
import org.slf4j.LoggerFactory
import java.time.Clock.system
import kotlin.system.exitProcess


@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class KimMainWindow(terminalSize: TerminalSize,
                    val kimCommandRegistryService: CommandRegistryService,
                    val textCommandParser: TextCommandParser,
                    val textFetchService: TextFetchService,
                    val insertionManagerService: InsertionManagerService,
                    val sessionManager: SessionManager,
                    val shortcutService: ShortcutService,
                    val undoService: UndoService) : BasicWindow(), CommandResultListener {

    private val logger = LoggerFactory.getLogger(KimMainWindow::class.java)

    private val mainPanel: MainPanel
    private val screenSizeAsColumns = terminalSize.columns
    private val screenSizeAsRows = terminalSize.rows
    private val LOGGER = LoggerFactory.getLogger(KimMainWindow::class.java)

    init {
        mainPanel = MainPanel(
                screenColumns = screenSizeAsColumns,
                screenRows = screenSizeAsRows,
                textFetchService = textFetchService,
                insertionManagerService = insertionManagerService,
                sessionManager = sessionManager,
                shortcutService = shortcutService,
                undoService = undoService)
        mainPanel.layoutManager = LinearLayout(Direction.VERTICAL)

        this.component = mainPanel
    }

    override fun close() {
        mainPanel.closeMainPanel()
        super.close()
    }

    fun updateMainPanelOnTerminalResized(updatedTerminalSize: TerminalSize?) {
        mainPanel.changeInputPanelTextBoxSize(updatedTerminalSize)
    }

    override fun reactOnExecutedCommand(executedCommand: ExecutedCommand) {
        if (executedCommand.commandStatus == ExecutedCommandStatus.OK) {
            when(executedCommand.commandName) {
                OpenFileExecutableCommandImpl.getCommandName() -> {
                    mainPanel.createNewInputPanelWithInputController(
                            InputTextController(
                                    controllerForUUID = sessionManager.createUUIDForFileName(executedCommand.commandReturnValue!!),
                                    filePath = executedCommand.commandReturnValue,
                                    textFetchService = textFetchService,
                                    insertionManagerService = insertionManagerService
                            ))
                }
                QuitExecutableCommandImpl.getCommandName() -> {
                    this.close()
                    exitProcess(0)
                }
            }
        }

    }

    override fun handleInput(key: KeyStroke?): Boolean {

        if(mainPanel.mode == Mode.KEY_COMMAND) {
            if (key?.character == ':') {
                this.textGUI.addWindowAndWait(TextCommandWindow(this,
                        mainPanel.getActiveInputPanelTextController(), kimCommandRegistryService, textCommandParser))
                return true
            }
        }

        return super.handleInput(key)
    }
}