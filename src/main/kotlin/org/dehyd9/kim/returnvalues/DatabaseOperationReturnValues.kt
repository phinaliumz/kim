package org.dehyd9.kim.returnvalues

enum class DatabaseOperationReturnValues {
    DATABASE_CREATION_OK,
    DATABASE_CREATION_FAILED,
    DATABASE_CREATED_TO_MEMORY
}