package org.dehyd9.kim.parsers

import org.dehyd9.kim.commands.interfaces.CommandResultListener
import org.dehyd9.kim.controllers.InputTextController

interface TextCommandParser {
    fun parseCommandAndExecute(commandString: String, commandResultListener: CommandResultListener,
                               inputTextController: InputTextController? = null)
}