package org.dehyd9.kim.models

import org.slf4j.LoggerFactory

class TextBuffer(private val bufferSize: Int = DEFAULT_BUFFER_64K) {

    private val LOGGER = LoggerFactory.getLogger(TextBuffer::class.java)

    private var listOfStrings = listOf<String>()
    private var buffer = listOf<Char>()


    companion object {
        private const val DEFAULT_BUFFER_64K = 64_000
    }

    fun fillBufferFromListOfChars(listOfChars: List<Char>) {
        buffer = listOfChars.asSequence()
                .take(bufferSize)
                .toList()
        val charArray = listOfChars.take(bufferSize).toCharArray()

        var strBuilder = StringBuilder()
        val listOfStrings = mutableListOf<String>()

        for(char in charArray) {
            strBuilder.append(char)

            if (strBuilder.contains(" ")) {
                listOfStrings.add(strBuilder.toString())
                strBuilder = StringBuilder()
            }
        }

        fillBufferFromListOfStrings(listOfStrings.toList())
    }

    fun fillBufferFromListOfStrings(listOfStrings: List<String>) {
        this.listOfStrings = listOfStrings
    }

    fun appendCharToBuffer(char: Char) {
        val mutableList = listOfStrings.toMutableList()
        mutableList.add(char.toString())
        listOfStrings = mutableList.toList()
    }

    fun appendStringToListOfStrings(line: String) {
        val mutableList = listOfStrings.toMutableList()
        mutableList.add(line)
        listOfStrings = mutableList.toList()
    }

    fun getImmutableLineBuffer(): List<String> {
        return listOfStrings
    }
}