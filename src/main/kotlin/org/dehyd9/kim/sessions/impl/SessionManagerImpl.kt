package org.dehyd9.kim.sessions.impl

import org.dehyd9.kim.persistence.services.DatabaseManager
import org.dehyd9.kim.sessions.Session
import org.dehyd9.kim.sessions.SessionManager
import org.dehyd9.kim.utils.generateTempFileName
import org.dehyd9.kim.utils.removeDatabaseFilesWithIdentifier
import org.slf4j.LoggerFactory
import java.util.*
import javax.inject.Inject

class SessionManagerImpl @Inject constructor(
        private val databaseManager: DatabaseManager
): SessionManager {

    private val randomNameForSession = generateRandomNameForSession()
    private val LOGGER = LoggerFactory.getLogger(SessionManagerImpl::class.java)

    init {
        databaseManager.initializeDatabaseForSession(randomNameForSession)
    }

    override fun createUUIDForFileName(fileName: String): UUID {
        val uuid = UUID.randomUUID()
        Session.UUIDToFilenameMap[uuid] = fileName
        return uuid
    }

    override fun getUUIDForUnsavedFile(): UUID {
        val uuid = UUID.randomUUID()
        Session.UUIDToFilenameMap[uuid] = generateRandomNameForNewFile()
        return uuid
    }

    override fun getFileNameForUUID(uuid: UUID): String? {
        return Session.UUIDToFilenameMap[uuid]
    }

    override fun getXorEncryptionKeyForSession(): String {
        return Session.xorEncryptionKeyForSession
    }

    override fun cleanUpWhenShuttingDown() {
        databaseManager.dropDatabaseForSession(randomNameForSession)
        removeDatabaseFilesWithIdentifier(randomNameForSession)
    }

    private fun generateRandomNameWithOptionalIdentifier(identifier: String? = null) = generateTempFileName(identifier)

    private fun generateRandomNameForSession(): String {
        return generateRandomNameWithOptionalIdentifier("session-")
    }

    private fun generateRandomNameForNewFile(): String {
        return generateRandomNameWithOptionalIdentifier()
    }
}