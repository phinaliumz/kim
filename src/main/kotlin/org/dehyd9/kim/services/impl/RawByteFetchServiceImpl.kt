package org.dehyd9.kim.services.impl

import org.dehyd9.kim.services.RawByteFetchService
import java.io.File
import java.io.IOException
import java.io.RandomAccessFile
import javax.inject.Inject

class RawByteFetchServiceImpl @Inject constructor()  : RawByteFetchService {

    companion object {
        const val READ_ONLY = "r"
    }

    override fun retrieveByteArrayFromPositionToPositionFromFilePath(bufferSize: Int, fromPosition: Long, toPosition: Long, fromFile: File): ByteArray {
        if (bufferSize <= 0 || bufferSize >= Int.MAX_VALUE) {
            throw IOException("Invalid buffersize: $bufferSize")
        }

        if(!fromFile.canRead()) {
            throw IOException("No permission to read ${fromFile.absolutePath}")
        }

        if (fromPosition > toPosition) {
            throw IOException("fromPosition must be smaller number than toPosition")
        }

        val byteArray = ByteArray(bufferSize)

        val randomAccessFileInputStream = RandomAccessFile(fromFile, READ_ONLY)
        randomAccessFileInputStream.seek(fromPosition)
        randomAccessFileInputStream.read(byteArray)

        randomAccessFileInputStream.close()

        return byteArray
    }
}