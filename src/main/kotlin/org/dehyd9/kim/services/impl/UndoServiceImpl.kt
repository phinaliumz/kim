package org.dehyd9.kim.services.impl

import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.services.UndoService
import java.util.*
import javax.inject.Inject

class UndoServiceImpl @Inject constructor(): UndoService {

    private val completedCommandsStack: ArrayDeque<ExecutedCommand> = ArrayDeque()
    private val redoStack: ArrayDeque<ExecutedCommand> = ArrayDeque()

    override fun redoLastCommand() {
        val lastExecutedCommand = this.popExecutedCommandFromRedoStackAndPushToCompletedCommandsStack()
    }

    override fun undoLastCommand() {
        val lastExecutedCommand = this.popExecutedCommandFromCompletedCommandsStackAndPushToRedoStack()
    }

    override fun pushExecutedCommandToCompletedCommandsStack(executedCommand: ExecutedCommand) {
        this.completedCommandsStack.addFirst(executedCommand)
    }

    private fun popExecutedCommandFromCompletedCommandsStackAndPushToRedoStack(): ExecutedCommand {
        val poppedExecutedCommand = this.completedCommandsStack.removeFirst()
        this.redoStack.addFirst(poppedExecutedCommand)
        return poppedExecutedCommand
    }

    private fun popExecutedCommandFromRedoStackAndPushToCompletedCommandsStack(): ExecutedCommand {
        val poppedExecutedCommand = this.redoStack.removeFirst()
        this.completedCommandsStack.addFirst(poppedExecutedCommand)
        return poppedExecutedCommand
    }
}