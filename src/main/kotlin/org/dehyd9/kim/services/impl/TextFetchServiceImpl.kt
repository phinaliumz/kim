package org.dehyd9.kim.services.impl

import com.github.teriks.img2a.AsciiPrinter
import com.github.teriks.img2a.ImageAsciiReader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.apache.commons.text.TextStringBuilder
import org.apache.tika.io.TikaInputStream
import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.AutoDetectParser
import org.apache.tika.parser.ParseContext
import org.apache.tika.parser.ParsingReader
import org.dehyd9.kim.services.FileService
import org.dehyd9.kim.services.RawByteFetchService
import org.dehyd9.kim.services.TextFetchService
import org.slf4j.LoggerFactory
import java.io.*
import javax.inject.Inject

class TextFetchServiceImpl @Inject constructor(
        private val fileService: FileService,
        private val rawByteFetchService: RawByteFetchService) : TextFetchService {

    private val LOGGER = LoggerFactory.getLogger(TextFetchServiceImpl::class.java)

    companion object {
        const val STREAM_ENDED = -1
    }

    override suspend fun retrieveThisManyCharsFromPositionToPositionFromFilePath(howManyChars: Int, fromPosition: Long, toPosition: Long, fromFilePath: String): Channel<Char> {
        val file = fileService.getFileFromPath(fromFilePath)
                ?: throw FileNotFoundException("Did not find file from $fromFilePath")
        val charChannel = Channel<Char>()

        val byteArray = rawByteFetchService.retrieveByteArrayFromPositionToPositionFromFilePath(
                howManyChars, fromPosition, toPosition, file
        )

        // if we are reading from start of the file, we must get the file type
        if (fromPosition == 0L) {
            //TODO
        }

        val byteArrayInputStream = ByteArrayInputStream(byteArray)
        val parser = AutoDetectParser()
        val metadata = Metadata()
        val parsingReader = ParsingReader(parser, byteArrayInputStream, metadata, ParseContext())
        val mediaType = TikaInputStream.get(byteArrayInputStream).use {
            parser.detector.detect(it, metadata)
        }
        val reader = when {
            mediaType.type == "image" -> {
                val imageAsciiImageRenderer = ImageAsciiReader(file)
                val asciiPrinter = AsciiPrinter(imageAsciiImageRenderer)
                asciiPrinter.print()

                val byteArrayStream = ByteArrayOutputStream()
                val outputStreamWriter = OutputStreamWriter(byteArrayStream)
                asciiPrinter.print(outputStreamWriter)
                outputStreamWriter.close()

                val byteArray = byteArrayStream.toByteArray()
                val byteArrayInputStream = ByteArrayInputStream(byteArray)
                InputStreamReader(byteArrayInputStream)
            }
            mediaType.type == "application" -> InputStreamReader(byteArrayInputStream)
            else -> parsingReader
        }

        reader.use {
            val intValue = it.read()
            while (intValue != -1) {
                charChannel.send(intValue.toChar())
            }
            charChannel.close()
        }

        return charChannel
    }

    override fun retrieveCharsFromFilePath(howManyChars: Int, fromFilePath: String): Channel<Char> {
        return this.retrieveCharsFromFileAtStartPosition(howManyChars, fromFilePath, startPosition = 0)
    }

    override fun retrieveCharsFromFilePathStartingAtIndex(
            howManyChars: Int, startIndex: Int, fromFilePath: String
    ): Channel<Char> {
        return this.retrieveCharsFromFileAtStartPosition(howManyChars, fromFilePath, startIndex)
    }

    override fun retrieveThisManyLinesFromFilePathStartingAtIndex(thisManyLines: Int, lineMaxLength: Int,
                                                                  startIndex: Int, fromFilePath: String): Channel<String> {
        return retrieveLinesFromFileAtStartPosition(thisManyLines, lineMaxLength, startIndex, fromFilePath)
    }

    override fun getReaderFromFileAtPath(fromFilePath: String): Reader? {

        val file = fileService.getFileFromPath(fromFilePath)
        file?.let {
            val fileInputStream = it.inputStream()
                val parser = AutoDetectParser()
                val metadata = Metadata()
                val parsingReader = ParsingReader(parser, fileInputStream, metadata, ParseContext())
                val mediaType = TikaInputStream.get(it.toPath()).use { tikaInputStream ->
                    parser.detector.detect(tikaInputStream, metadata)
                }

                return when {
                    mediaType.type == "image" -> {
                        val imageAsciiImageRenderer = ImageAsciiReader(file)
                        val asciiPrinter = AsciiPrinter(imageAsciiImageRenderer)
                        asciiPrinter.print()

                        val byteArrayStream = ByteArrayOutputStream()
                        val outputStreamWriter = OutputStreamWriter(byteArrayStream)
                        asciiPrinter.print(outputStreamWriter)
                        outputStreamWriter.close()

                        val byteArray = byteArrayStream.toByteArray()
                        val byteArrayInputStream = ByteArrayInputStream(byteArray)
                        InputStreamReader(byteArrayInputStream)
                    }
                    mediaType.type == "application" -> InputStreamReader(fileInputStream)
                    else -> parsingReader
                }
        }

        return null
    }

    private fun getReaderForFilePath(forFilePath: String): Reader? {
        val file = fileService.getFileFromPath(forFilePath)

        file?.let {
            val fileInputStream = FileInputStream(it)
            val parser = AutoDetectParser()
            val metadata = Metadata()
            val parsingReader = ParsingReader(parser, fileInputStream, metadata, ParseContext())

            val mediaType = TikaInputStream.get(it.toPath()).use { tikaInputStream ->
                parser.detector.detect(tikaInputStream, metadata)
            }

            val reader: Reader = when {
                mediaType.type == "image" -> {
                    val imageAsciiImageRenderer = ImageAsciiReader(file)
                    val asciiPrinter = AsciiPrinter(imageAsciiImageRenderer)
                    asciiPrinter.print()

                    val byteArrayStream = ByteArrayOutputStream()
                    val outputStreamWriter = OutputStreamWriter(byteArrayStream)
                    asciiPrinter.print(outputStreamWriter)
                    outputStreamWriter.close()

                    val byteArray = byteArrayStream.toByteArray()
                    val byteArrayInputStream = ByteArrayInputStream(byteArray)
                    InputStreamReader(byteArrayInputStream)
                }
                mediaType.type == "application" -> InputStreamReader(FileInputStream(it))
                else -> parsingReader
            }

            return reader;
        }

        return null
    }

    private fun retrieveLinesFromFileAtStartPosition(howManyLines: Int, lineMaxLength: Int, startPosition: Int,
                                                     fromFilePath: String
                                                     ): Channel<String> {

        val lineChannel = Channel<String>()

        if (!fileService.isFileExists(fromFilePath)) {
            lineChannel.close()
            return lineChannel
        }

        val reader = getReaderForFilePath(fromFilePath)

        reader?.let {
            var lineCounter = 0
            var charsAdded = 0
            var lineBuilder = TextStringBuilder()
            var readInt: Int

            GlobalScope.launch {
                val bufferedReader = BufferedReader(it)
                bufferedReader.use {
                    bufferedReader.skip(startPosition.toLong())
                    readInt = bufferedReader.read()
                    while(lineCounter <= howManyLines && readInt != STREAM_ENDED) {
                        val readChar = readInt.toChar()
                        lineBuilder.append(readChar)
                        charsAdded++
                        if (lineBuilder.contains(System.lineSeparator()) || charsAdded >= lineMaxLength) {
                            lineChannel.send(lineBuilder.toString())
                            lineBuilder = TextStringBuilder()
                            charsAdded = 0
                            lineCounter++
                        }
                        readInt = bufferedReader.read()
                    }

                    if (lineBuilder.isNotEmpty()) {
                        lineChannel.send(lineBuilder.toString())
                    }
                }
                lineChannel.close()
            }
        }

        return lineChannel
    }

    private fun retrieveCharsFromFileAtStartPosition(howManyChars: Int, fromFilePath: String, startPosition: Int): Channel<Char> {

        val channel = Channel<Char>()

        if (!fileService.isFileExists(fromFilePath)) {
            channel.close()
            return channel
        }

        val file = fileService.getFileFromPath(fromFilePath)

        file?.let {
            val fileInputStream = FileInputStream(it)
            val parser = AutoDetectParser()
            val metadata = Metadata()
            val parsingReader = ParsingReader(parser, fileInputStream, metadata, ParseContext())

            val mediaType = TikaInputStream.get(it.toPath()).use { tikaInputStream ->
                parser.detector.detect(tikaInputStream, metadata)
            }

            val reader: Reader = when {
                mediaType.type == "image" -> {
                    val imageAsciiImageRenderer = ImageAsciiReader(file)
                    val asciiPrinter = AsciiPrinter(imageAsciiImageRenderer)
                    asciiPrinter.print()

                    val byteArrayStream = ByteArrayOutputStream()
                    val outputStreamWriter = OutputStreamWriter(byteArrayStream)
                    asciiPrinter.print(outputStreamWriter)
                    outputStreamWriter.close()

                    val byteArray = byteArrayStream.toByteArray()
                    val byteArrayInputStream = ByteArrayInputStream(byteArray)
                    InputStreamReader(byteArrayInputStream)
                }
                mediaType.type == "application" -> InputStreamReader(FileInputStream(it))
                else -> parsingReader
            }

            GlobalScope.launch {
                val bufferedReader = BufferedReader(reader)
                bufferedReader.use {
                    bufferedReader.skip(startPosition.toLong())
                    var readChars = 0
                    while (readChars <= howManyChars) {
                        val readInt = bufferedReader.read()
                        if (readInt == STREAM_ENDED) {
                            readChars = howManyChars
                        } else {
                            val readChar = readInt.toChar()
                            channel.send(readChar)
                            readChars++
                        }

                    }
                }
                channel.close()
            }
        }

        return channel
    }
}