package org.dehyd9.kim.services.impl

import kotlinx.coroutines.yield
import org.apache.commons.io.FileUtils
import org.dehyd9.kim.gui.enums.ControlCharacter
import org.dehyd9.kim.persistence.dto.InsertionDTO
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.SaveFileService
import org.slf4j.LoggerFactory
import java.io.*
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.util.*
import javax.inject.Inject

class SaveFileServiceImpl @Inject constructor(
        val insertionManagerService: InsertionManagerService) :SaveFileService {

    private val LOGGER = LoggerFactory.getLogger(SaveFileServiceImpl::class.java)

    companion object {
        const val READ_BYTES_BUFFER = 5000
        const val MAX_SIZE_OF_SEQUENCE_OF_INSERTIONS = 5000
        const val EMPTY_STRING = ""
    }

    override fun saveFile(file: File, fileUUID: UUID): Boolean {

        LOGGER.debug("Filename -> ${file.name} and uuid -> $fileUUID")

        val insertionsListForUUID = insertionManagerService.retrieveListOfInsertionsForUUID(fileUUID) ?: return false

        if(insertionsListForUUID.isEmpty() || !file.canWrite()) {
            return false
        }

        val pathToFile = file.toPath()
        val fileAttributes = Files.readAttributes(pathToFile, BasicFileAttributes::class.java)
        val sizeOfFile = fileAttributes.size()

        val isEmptyFile = sizeOfFile == 0L

        return if(isEmptyFile) {
            writeTextToEmptyFile(file, insertionsListForUUID)
        } else {
            writeTextToExistingFile(file, insertionsListForUUID)
        }
    }

    private fun writeTextToExistingFile(file: File, insertionDtoList: List<InsertionDTO>): Boolean {

        var fileIsReady = false
        var readOffset = 0
        var writeOffset = 0
        var insertionListIndex = 0

        while(!fileIsReady) {

            val bis = BufferedInputStream(FileInputStream(file))
            val byteArray = ByteArray(READ_BYTES_BUFFER)

            bis.use {
                it.read(byteArray, readOffset, READ_BYTES_BUFFER)
            }

            val stringBuilder = StringBuilder()

            readOffset += READ_BYTES_BUFFER

            while ( insertionListIndex<= READ_BYTES_BUFFER && insertionListIndex <= insertionDtoList.size) {
                stringBuilder.append(insertionDtoList[insertionListIndex].insertedValue)
                insertionListIndex++
            }

            val bos = BufferedOutputStream(FileOutputStream(file))
            val writeByteArray = stringBuilder.toString().toByteArray(Charset.defaultCharset())
            bos.use {
                bos.write(writeByteArray, writeOffset, writeByteArray.size)
            }

            writeOffset += writeByteArray.size

            fileIsReady = insertionListIndex >= insertionDtoList.size
        }

        return true
    }

    private fun writeTextToEmptyFile(file: File, insertionDtoList: List<InsertionDTO>): Boolean {

        insertionDtoList.asSequence()
                .constrainOnce()
                .chunked(MAX_SIZE_OF_SEQUENCE_OF_INSERTIONS)
                .forEach {
                    val value = it.asSequence()
                            .map { insertion ->
                                if (insertion.insertedValue == "NEW_LINE") {
                                    System.lineSeparator()
                                } else {
                                    insertion.insertedValue
                                }
                            }
                            .fold(EMPTY_STRING, { acc: String, s: String -> acc + s })
                    FileUtils.writeStringToFile(file, value, Charset.defaultCharset())
                }

        return true
    }
}