package org.dehyd9.kim.utils

import org.apache.commons.io.FileUtils
import org.dehyd9.kim.utils.KimFileUtils.KIM_SESSIONS_DIRECTORY
import java.nio.file.FileSystems
import java.nio.file.Path

private val separator = FileSystems.getDefault().separator
private val userHomeDirectory = FileUtils.getUserDirectoryPath()

object KimFileUtils {
    const val KIM_SESSIONS_DIRECTORY = "kim-sessions"
}

fun removeDatabaseFilesWithIdentifier(identifier: String) {
    val path = Path.of(userHomeDirectory, KIM_SESSIONS_DIRECTORY, identifier)
    FileUtils.deleteDirectory(path.toFile())
}

fun generateDatabaseFilenameToUserHomeDirectory(filename: String): String {
    val databaseFileNameBuilder = StringBuilder()
            .append(userHomeDirectory)
            .append(separator)
            .append(KIM_SESSIONS_DIRECTORY)
            .append(separator)
            .append(filename)
            .append(separator)
            .append(filename)
    return databaseFileNameBuilder.toString()
}