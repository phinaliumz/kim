package org.dehyd9.kim.commands.interfaces.impl

import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.enums.SystemCommand
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.sessions.SessionManager
import javax.inject.Inject

class QuitExecutableCommandImpl @Inject constructor(private val sessionManager: SessionManager) : ExecutableCommand {

    companion object {
        fun getCommandName(): String {
            return SystemCommand.QUIT.systemCommandName
        }
    }

    override fun executeWithParameters(commandParameters: Set<CommandParameter>): ExecutedCommand {
        sessionManager.cleanUpWhenShuttingDown()
        return ExecutedCommand(getCommandName(), commandStatus = ExecutedCommandStatus.OK)
    }

    override fun getCommandName(): String {
        return QuitExecutableCommandImpl.getCommandName()
    }

}