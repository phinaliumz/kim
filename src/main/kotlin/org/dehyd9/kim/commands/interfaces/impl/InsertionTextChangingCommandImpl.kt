package org.dehyd9.kim.commands.interfaces.impl

import org.dehyd9.kim.commands.interfaces.TextChangingCommand
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.persistence.entities.Insertion
import java.util.*

class InsertionTextChangingCommandImpl : TextChangingCommand {

    private lateinit var insertion: Insertion

    companion object {
        private const val commandName = "org.dehyd9.kim.commmands.InsertionCommand"
        private const val ROW_INDEX_IN_PARAMETER_SET = 0
        private const val COLUMN_INDEX_IN_PARAMETER_SET = 1
        private const val CHARACTER_INDEX_IN_PARAMETER_SET = 2
        private const val UUID_INDEX_IN_PARAMETER_SET= 3
        private const val FIRST_CHARACTER_POSITION = 0
    }

    override fun executeWithParameters(commandParameters: Set<CommandParameter>): ExecutedCommand {
        TODO()

        /*
        * There should be three parameters; inserted character, its row and column
         */
        /*if (commandParameters.size != 3) {
            return ExecutedCommand(commandName, CommandReturnValue.WRONG_PARAMETERS)
        }

        val rowValue = this.getRowFromCommandParameterSet(commandParameters) ?:
            return ExecutedCommand(commandName, CommandReturnValue.WRONG_PARAMETERS)
        val columnValue = this.getColumnFromCommandParameterSet(commandParameters) ?:
            return ExecutedCommand(commandName, CommandReturnValue.WRONG_PARAMETERS)
        val character = this.getCharacterFromCommandParameterSet(commandParameters)
        val uuid = this.getUUIDFromCommandParameterSet(commandParameters) ?:
            return ExecutedCommand(commandName, CommandReturnValue.WRONG_PARAMETERS)
        insertion = Insertion(

                insertionRowNumber = rowValue, insertionColumnNumber = columnValue,
                uuid = uuid
        )

        val commandReturnObject = OldCommandReturnObject(insertion)


        return ExecutedCommand(commandName = commandName, commandReturnValue =  CommandReturnValue.OK,
                commandReturnObject = commandReturnObject)
                */
    }

    override fun getCommandName(): String {
        return commandName
    }

    private fun getUUIDFromCommandParameterSet(commandParameters: Set<CommandParameter>): UUID? {
        val uuidParameterAsString = commandParameters.elementAt(
                UUID_INDEX_IN_PARAMETER_SET).parameterValue?.getParameterValueAsString()
        return UUID.fromString(uuidParameterAsString)
    }

    private fun getCharacterFromCommandParameterSet(commandParameters: Set<CommandParameter>): Char? {
        val characterParameterAsString = commandParameters.elementAt(
                CHARACTER_INDEX_IN_PARAMETER_SET).parameterValue?.getParameterValueAsString()
        return characterParameterAsString?.get(FIRST_CHARACTER_POSITION)
    }

    private fun getColumnFromCommandParameterSet(commandParameters: Set<CommandParameter>): Long? {
        return this.getParameterAsLongValueFromParameterSetAtIndex(commandParameters, COLUMN_INDEX_IN_PARAMETER_SET)
    }

    private fun getRowFromCommandParameterSet(commandParameters: Set<CommandParameter>): Long? {
        return this.getParameterAsLongValueFromParameterSetAtIndex(commandParameters, ROW_INDEX_IN_PARAMETER_SET)
    }

    private fun getParameterAsLongValueFromParameterSetAtIndex(commandParameters: Set<CommandParameter>, index: Int): Long? {
        val parameterAsStringValue = commandParameters.elementAt(index).parameterValue?.getParameterValueAsString()
        return parameterAsStringValue?.toLongOrNull()
    }
}