package org.dehyd9.kim.commands.parameters

data class CommandParameter(val parameterName :String, val parameterValue :CommandParameterValue? = null)