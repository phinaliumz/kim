package org.dehyd9.kim.commands.results

import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.parameters.CommandParameter

data class ExecutedCommand(val commandName: String, val commandStatus: ExecutedCommandStatus,
                           val commandReturnValue: String? = null, val commandParameters: Set<CommandParameter>? = null)