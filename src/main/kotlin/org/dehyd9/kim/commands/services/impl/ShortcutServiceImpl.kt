package org.dehyd9.kim.commands.services.impl

import org.dehyd9.kim.commands.enums.SystemCommand
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.services.ShortcutService
import javax.inject.Inject

class ShortcutServiceImpl @Inject constructor(kimCommandRegistryService: CommandRegistryService): ShortcutService {

    private val systemShortCutCommandMap: Map<String, ExecutableCommand?> = mapOf(
            //TODO -> "u" to kimCommandRegistryService.getKimCommandForName(SystemCommand.UNDO_COMMAND.systemCommandName),
            "i" to kimCommandRegistryService.getKimCommandForName(SystemCommand.INSERT_MODE.systemCommandName),
            "o" to kimCommandRegistryService.getKimCommandForName(SystemCommand.OPEN_FILE.systemCommandName),
            "q" to kimCommandRegistryService.getKimCommandForName(SystemCommand.QUIT.systemCommandName),
            "w" to kimCommandRegistryService.getKimCommandForName(SystemCommand.SAVE_FILE.systemCommandName)
    )

    override fun getCommandNameFromShortCutKey(shortCutKey: String): String? {
        if (systemShortCutCommandMap.containsKey(shortCutKey)) {
            return this.getSystemCommandFromShortCutKey(shortCutKey)?.getCommandName()
        }
        return null
    }

    private fun getSystemCommandFromShortCutKey(shortCutKey: String): ExecutableCommand? {
        return this.systemShortCutCommandMap[shortCutKey]
    }
}