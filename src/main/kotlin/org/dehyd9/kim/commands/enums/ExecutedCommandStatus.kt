package org.dehyd9.kim.commands.enums

enum class ExecutedCommandStatus {
    OK, ERROR, WRONG_PARAMETERS
}