package org.dehyd9.kim.commands.registry.interfaces.impl

import org.dehyd9.kim.commands.enums.SystemCommand
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.interfaces.impl.*
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.services.FileService
import org.dehyd9.kim.services.SaveFileService
import org.dehyd9.kim.sessions.SessionManager
import javax.inject.Inject

class CommandRegistryServiceImpl @Inject constructor(private val fileService: FileService,
                                                     private val saveFileService: SaveFileService,
                                                     private val sessionManager: SessionManager): CommandRegistryService {

    private val kimCommands = HashMap<String, ExecutableCommand>()
    private val kimSystemCommands = HashMap<SystemCommand, ExecutableCommand>()
    private val kimSystemCommandNames = HashMap<String, SystemCommand>()

    init {
        this.loadCommands()
        this.loadCommandNames()
    }

    /*
    * TODO: ExecutableCommand should be located by their name - so they should be registered to system someway?
     */
    override fun getKimCommandMap(): Map<String, ExecutableCommand> {
        return this.kimCommands
    }

    override fun getKimCommandForName(commandName: String): ExecutableCommand? {
        if (this.kimSystemCommandNames.containsKey(commandName)) {
            return this.kimSystemCommands[kimSystemCommandNames[commandName]]
        }
        return this.kimCommands[commandName]
    }

    private fun loadCommandNames() {
        this.kimSystemCommandNames[SystemCommand.QUIT.systemCommandName] = SystemCommand.QUIT
        this.kimSystemCommandNames[SystemCommand.INSERT_MODE.systemCommandName] = SystemCommand.INSERT_MODE
        this.kimSystemCommandNames[SystemCommand.OPEN_FILE.systemCommandName] = SystemCommand.OPEN_FILE
        this.kimSystemCommandNames[SystemCommand.UNDO_COMMAND.systemCommandName] = SystemCommand.UNDO_COMMAND
        this.kimSystemCommandNames[SystemCommand.SAVE_FILE.systemCommandName] = SystemCommand.SAVE_FILE
    }

    private fun loadCommands() {
        this.kimSystemCommands[SystemCommand.QUIT] = QuitExecutableCommandImpl(sessionManager = sessionManager)
        this.kimSystemCommands[SystemCommand.INSERT_MODE] = InputModeExecutableCommandImpl()
        this.kimSystemCommands[SystemCommand.OPEN_FILE] = OpenFileExecutableCommandImpl(fileService = fileService)
        this.kimSystemCommands[SystemCommand.SAVE_FILE] = SaveFileExecutableCommandImpl(saveFileService = saveFileService)
        this.kimSystemCommands[SystemCommand.UNDO_COMMAND] = NotFoundExecutableCommandImpl()

    }
}