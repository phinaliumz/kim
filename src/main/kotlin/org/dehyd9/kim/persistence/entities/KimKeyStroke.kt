package org.dehyd9.kim.persistence.entities

import com.googlecode.lanterna.input.KeyType
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import org.dehyd9.kim.persistence.dto.KimKeyStrokeDTO
import java.util.*

@DatabaseTable(tableName = "kimkeystroke")
data class KimKeyStroke(
        @DatabaseField(generatedId = true) val id: Long? = null,
        @DatabaseField val forUuid: UUID? = null,
        @DatabaseField val keyType: KeyType? = null,
        @DatabaseField val character: Char? = null,
        @DatabaseField val ctrlDown: Boolean? = null,
        @DatabaseField val altDown: Boolean? = null,
        @DatabaseField val shiftDown: Boolean? = null,
        @DatabaseField val eventTime: Long? = null
        ) {
    fun toKimKeyStrokeDTO(): KimKeyStrokeDTO = KimKeyStrokeDTO(
            forUuid = forUuid,
            keyType = keyType,
            character = character,
            ctrlDown = ctrlDown,
            altDown = altDown,
            shiftDown = shiftDown,
            eventTime = eventTime

    )
}