package org.dehyd9.kim.persistence.entities

import com.j256.ormlite.field.DataType
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import org.dehyd9.kim.commands.parameters.CommandParameterValue
import org.dehyd9.kim.persistence.dto.InsertionDTO
import java.nio.charset.Charset
import java.util.*

@DatabaseTable(tableName = "insertion")
data class Insertion(
        @DatabaseField(generatedId = true) val id: Long? = null,
        @DatabaseField val row: Int = -1,
        @DatabaseField val column: Int = -1,
        @DatabaseField(dataType = DataType.BYTE_ARRAY) val insertedValue: ByteArray = ByteArray(0),
        @DatabaseField val isCommandKeyCodeInsertion: Boolean = false,
        @DatabaseField val uuid: UUID = UUID.randomUUID(),
        @DatabaseField val dateAsMilliseconds: Long = 0L
): CommandParameterValue {

    override fun getParameterValueAsString(): String {
        return insertedValue.toString()
    }

    companion object {
        const val ID_FIELDNAME = "id"
        const val UUID_FIELDNAME = "uuid"
    }

    fun toInsertionDTO(): InsertionDTO = InsertionDTO(
            row = row,
            column = column,
            insertedValue = insertedValue.toString(Charset.defaultCharset()),
            isCommandKeyCodeInsertion = isCommandKeyCodeInsertion,
            uuid = uuid,
            dateAsMilliseconds = dateAsMilliseconds)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Insertion

        if (id != other.id) return false
        if (row != other.row) return false
        if (column != other.column) return false
        if (!insertedValue.contentEquals(other.insertedValue)) return false
        if (isCommandKeyCodeInsertion != other.isCommandKeyCodeInsertion) return false
        if (uuid != other.uuid) return false
        if (dateAsMilliseconds != other.dateAsMilliseconds) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + row
        result = 31 * result + column
        result = 31 * result + insertedValue.contentHashCode()
        result = 31 * result + isCommandKeyCodeInsertion.hashCode()
        result = 31 * result + uuid.hashCode()
        result = 31 * result + dateAsMilliseconds.hashCode()
        return result
    }
}