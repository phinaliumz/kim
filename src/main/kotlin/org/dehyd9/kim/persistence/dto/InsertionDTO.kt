package org.dehyd9.kim.persistence.dto

import org.dehyd9.kim.persistence.entities.Insertion
import java.nio.charset.Charset
import java.util.*

data class InsertionDTO(
        val row: Int,
        val column: Int,
        val insertedValue: String,
        val uuid: UUID,
        val dateAsMilliseconds: Long,
        val isCommandKeyCodeInsertion: Boolean
) {
    fun toInsertion() = Insertion(
            row = row,
            column = column,
            insertedValue = insertedValue.toByteArray(Charset.defaultCharset()),
            uuid = uuid,
            dateAsMilliseconds = dateAsMilliseconds,
            isCommandKeyCodeInsertion = isCommandKeyCodeInsertion)
}
