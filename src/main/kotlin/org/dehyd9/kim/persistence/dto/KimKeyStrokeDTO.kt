package org.dehyd9.kim.persistence.dto

import com.googlecode.lanterna.input.KeyType
import org.dehyd9.kim.persistence.entities.KimKeyStroke
import java.util.*

data class KimKeyStrokeDTO(
        val forUuid: UUID?,
        val keyType: KeyType?,
        val character: Char?,
        val ctrlDown: Boolean?,
        val altDown: Boolean?,
        val shiftDown: Boolean?,
        val eventTime: Long?
) {
    fun toKimKeyStroke(): KimKeyStroke = KimKeyStroke(
            forUuid = forUuid,
            keyType = keyType,
            character = character,
            ctrlDown = ctrlDown,
            altDown = altDown,
            shiftDown = shiftDown,
            eventTime = eventTime
    )
}