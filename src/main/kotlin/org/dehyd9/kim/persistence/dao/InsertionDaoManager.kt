package org.dehyd9.kim.persistence.dao

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.support.ConnectionSource
import org.dehyd9.kim.persistence.entities.Insertion
import org.dehyd9.kim.persistence.entities.KimKeyStroke


fun getInsertionDao(connectionSource: ConnectionSource) :Dao<Insertion, Long> {
    return DaoManager.createDao(connectionSource, Insertion::class.java)
}

fun getKimKeyStrokeDao(connectionSource: ConnectionSource) :Dao<KimKeyStroke, Long> {
    return DaoManager.createDao(connectionSource, KimKeyStroke::class.java)
}