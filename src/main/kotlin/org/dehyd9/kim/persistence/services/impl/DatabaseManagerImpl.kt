package org.dehyd9.kim.persistence.services.impl

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils.createTable
import com.j256.ormlite.table.TableUtils.dropTable
import org.dehyd9.kim.persistence.entities.KimKeyStroke
import org.dehyd9.kim.persistence.services.DatabaseManager
import org.dehyd9.kim.returnvalues.DatabaseOperationReturnValues
import org.dehyd9.kim.utils.generateDatabaseFilenameToUserHomeDirectory
import org.slf4j.LoggerFactory
import java.sql.SQLException
import javax.inject.Inject

class DatabaseManagerImpl @Inject constructor() : DatabaseManager {

    private val LOGGER = LoggerFactory.getLogger(DatabaseManagerImpl::class.java)

    companion object {
        const val CONNECTION_TO_FILE_STRING_PREFIX = "jdbc:hsqldb:file:"
        const val CONNECTION_STRING_POSTFIX = ";hsqldb.default_table_type=cached"
        const val CONNECTION_TO_MEMORY_STRING_PREFIX = "jdbc:hsqldb:mem"
    }

    private lateinit var databaseConnectionForSession: JdbcConnectionSource

    override fun initializeDatabaseForSession(databaseFilename: String): DatabaseOperationReturnValues {
        return createTableForFilename(databaseFilename)
    }

    override fun getDatabaseConnectionForSession(): JdbcConnectionSource? = databaseConnectionForSession

    override fun dropDatabaseForSession(databaseFilename: String): Int =
            dropTable<KimKeyStroke, Long>(databaseConnectionForSession, KimKeyStroke::class.java, false)

    private fun createTableForFilename(filename: String): DatabaseOperationReturnValues  {
        val databaseFileNameBuilder = StringBuilder()
                .append(CONNECTION_TO_FILE_STRING_PREFIX)
                .append(generateDatabaseFilenameToUserHomeDirectory(filename))
                .append(CONNECTION_STRING_POSTFIX)
        try {
            databaseConnectionForSession = JdbcConnectionSource(databaseFileNameBuilder.toString())
            //createTable(databaseConnectionForSession, Insertion::class.java)
            createTable(databaseConnectionForSession, KimKeyStroke::class.java)
            return DatabaseOperationReturnValues.DATABASE_CREATION_OK
        } catch (ssse: SQLException) {
            LOGGER.error("Failed to create database file: ${ssse.localizedMessage}")
            LOGGER.error("Trying to create database to memory.")
            try {
                databaseConnectionForSession = JdbcConnectionSource("$CONNECTION_TO_MEMORY_STRING_PREFIX$filename$CONNECTION_STRING_POSTFIX")
                createTable(databaseConnectionForSession, KimKeyStroke::class.java)
                return DatabaseOperationReturnValues.DATABASE_CREATED_TO_MEMORY
            } catch (se: SQLException) {
                LOGGER.error("Failed to create database to memory")
                return DatabaseOperationReturnValues.DATABASE_CREATION_FAILED
            }
        }
    }
}