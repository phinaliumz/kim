package org.dehyd9.kim.persistence.services

import org.dehyd9.kim.persistence.dto.InsertionDTO
import org.dehyd9.kim.persistence.dto.KimKeyStrokeDTO
import java.util.*

interface InsertionManagerService {
    fun persistInsertion(insertionDTO: InsertionDTO)
    fun retrieveListOfInsertionsForUUID(uuid: UUID? = null) : List<InsertionDTO>?
    fun retrieveLastInsertionForUUID(uuid: UUID? = null) : InsertionDTO?
    fun persistKeyStroke(kimKeyStrokeDTO: KimKeyStrokeDTO)
    fun retrieveListOfKimKeyStrokesForUUID(uuid: UUID? = null) : List<KimKeyStrokeDTO>?
    fun retrieveLastKimKeyStrokeForUUID(uuid: UUID? = null) : KimKeyStrokeDTO?
}