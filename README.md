This is my Vim-clone in Kotlin.

To test a big, random file you can use `dd` command, like for example
```
dd if=/dev/urandom of=big-file-test.txt bs=40M count=100
```

This generates about 3,2G file.